public class Cilindro extends Circulo implements Figura3D { //substitua "..." pelo codigo Java adequado
    protected double altura;
    
    public Cilindro(double raio, double altura){
        super(raio);
        this.altura = altura;
    }
    
    public double getVolume(){
        return super.getArea() * this.altura;
    }
    
    public double getArea(){
        return 2 * super.getArea() + this.altura * super.getPerimetro();
    }
    
    //AVISO: Crie um construtor para esta classe. O construtor recebe um raio e uma altura como parametros (nesta ordem). O tipo do raio e altura deve ser double
}
