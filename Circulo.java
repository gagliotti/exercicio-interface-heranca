public class Circulo implements Figura2D { //substitua "..." pelo codigo Java adequado
  	protected double raio;
  	
  	//construtor
  	public Circulo(double raio){
  	     this.raio = raio;   
  	}
  	
  	public double getArea(){
  	    return Math.PI * this.raio * this.raio;  
  	}
  	
	public double getPerimetro(){
	    return 2 * Math.PI * this.raio;
	}
}
